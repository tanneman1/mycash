import { createStore } from 'vuex'
import axios from "axios";
import { parseRbc } from "./parser";

function filterDuplicates() {
  const existing = [];
  return (row) => {
    if (!existing.includes(row.id)) {
      existing.push(row.id);
      return true;
    }
    return false;
  };
}

export default createStore({
  state: {
    loading: false,
    data: [],
    error: null,
    url: 'https://192.168.1.10/cashflow/data.php?data=rbc',
  },
  mutations: {
    setUrl(state, url) {
      state.url = url
    },
    data(state, data) {
      state.data = data
    },
    error(state, error) {
      state.error = error
    },
    loading(state, loading) {
      state.loading = loading
    }
  },
  actions: {
    loadData(context) {
      context.commit('data', [])
      context.commit('loading', true)
      axios
        .get(context.state.url)
        .then(function (response) {
          context.commit('loading', false)

          const data = response.data
            .slice(1)
            .map((row) => parseRbc(row))
            .filter(filterDuplicates())
            .sort((a, b) => b.date - a.date);
            context.commit('data', data)
        })
        .catch(function (error) {
          context.commit('loading', false)
          context.commit('error', error)

          console.error({ error });
        });
      // context.commit('increment')
    }
  }
})
