export function sum(data) {
  return data.reduce((acc, item) => {
    return acc + item.amountInCents
  }, 0)
}

export function calculateStats(data) {
  return {
    sum: sum(data)
  }
}