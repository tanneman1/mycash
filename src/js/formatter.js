const decimal = ',';

export function valuta(value) {
  // if (value < 10) {
  //   return `0${decimal}0${value}`
  // }
  // if (value < 100) {
  //   return `0${decimal}${value}`
  // }
  const valueStr = new String(value).padStart(3, '0');
  return valueStr.slice(0, -2) + decimal + valueStr.slice(-2) + ' $';
}