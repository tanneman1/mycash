export function parseRbc(row) {
  const account = row[1]
  const date = new Date(row[2]);
  const text1 = row[4];
  const text2 = row[5];
  const amount = row[6];
  const id = JSON.stringify(row);
  const dateDisplay = date.toLocaleDateString("en-CA");
  const amountInCents = parseInt(row[6].replace( /\./g, ''));
  return {
    id,
    account,
    date,
    dateDisplay,
    description: { text1, text2 },
    amount,
    amountInCents,
    isNegative: amountInCents < 0,
    _raw: row,
    matches: (text) => {
      if (text == "") {
        return true
      }
      const regex = new RegExp(text, "gi");
      return [account, dateDisplay, text1, text2, amount].some(a => a.match(regex))
    },
    matchesYear: (years) => {
      return years.length === 0 ? true : years.includes(date.getFullYear());
    }
  };
}