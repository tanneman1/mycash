A cashview app that shows my transactions. Goal is to learn Vue3 and its composition API and have an updated version of my old cashview app (Vue). Accesible without data on http://cashview.netlify.app/.

# run dev server
```
npm run dev
```

# run build 
```
npm run build
```